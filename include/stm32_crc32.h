/**
 * @file stm32_crc32.h
 *
 * @brief CRC-32, compatible with STM32 hardware implementation.
 *
 * @date Nov 6, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

#ifndef STM_CRC32_H_
#define STM_CRC32_H_

/**
 * Computes CRC-32 checksum in a way that should be compatible
 * with that on a STM32 processor.
 */
unsigned int crc32( unsigned int crc, unsigned int data );

/**
 * Calculates CRC-32 on a block of data.
 */
unsigned int crc32_block( unsigned int crc, unsigned int *data, unsigned int length );

#endif /* STM_CRC32_H_ */
