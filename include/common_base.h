/**
 * @file common_base.h
 *
 * @brief A common header for base definitions and types.
 * @note This file should not contain any platform specific code.
 *
 * @date Jun 25, 2012
 * @author Indrek, Henri
 * @author ESTCube-1 Team
 * @copyright Tartu Observatory
 */

#ifndef COMMON_BASE_H_
#define COMMON_BASE_H_

#include <stdint.h>
#include <stddef.h>

#ifndef REDEF_BOOL
#define true 1
#define false 0
#define TRUE 1
#define FALSE 0
#define DAYSEC 86400
#endif // REDEF_BOOL

#ifndef NULL
	#define NULL 0
#endif

//! Absolute value
#ifndef EC_ABS
	#define EC_ABS( x )	( (x) > 0 ? (x) : -(x) )
#endif

/**
 * Signum
 * @return 1 on positive or zero, -1 on negative
 */
#ifndef EC_SIGN
	#define EC_SIGN( x )	( (x) > 0 ? 1 : -1 )
#endif

/**
 * Minimum of 2 values
 */
#ifndef EC_MIN
	#define EC_MIN( x, y )	( (x) < (y) ? (x) : (y) )
#endif

/**
 * Maximum of 2 values
 */
#ifndef EC_MAX
	#define EC_MAX( x, y )	( (x) > (y) ? (x) : (y) )
#endif

/**
 * Rounds a floating point number to a decimal
 */
#ifndef EC_ROUND
	#define EC_ROUND( f )	( ( (f) > 0.0 ) ? floor( (f) + 0.5 ) : ceil( (f) - 0.5 ) )
#endif

//! Aligns x to a multiple of m
#define EC_ALIGN( x, m )	( ( ( x / m ) + ( x % m != 0 ) ) * m )

/**
 * Rounds x up to the next highest power of 2.
 */
uint32_t ec_round_up_pow2( uint32_t x );

/**
 * Rounds x to the closest power of 2 value.
 */
uint32_t ec_round_pow2( uint32_t x );

//! Weak function attribute
#define WEAK	__attribute__((weak))


//! \bug Unresolved reference to DBG_ASSERT
#ifndef DBG_ASSERT
	#ifdef DEBUG
		#include "ex_handling.h"
		//! \todo Support for module numbers
		#define DBG_ASSERT(x)	{ if ( !x ) THROW( DBG_EX_ASSERT_FAIL ); }
	#else
		#define DBG_ASSERT(x)
	#endif
#endif

#define ec_PI 3.1415926535897932

typedef unsigned char bool;

#endif /* COMMON_BASE_H_ */
