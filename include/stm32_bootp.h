/**
 * @file stm32_bootp.h
 *
 * @brief STM32 internal bootloader protocol.
 * Well, a really basic implementation of it.
 *
 * @date Oct 3, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

/**
 * @note Limits for USART configuration:
 * Baud rate: 1200 .. 115200
 * Start bits: 1
 * Parity: Even
 * Stop bits: 1
 */

#ifndef STM32_BOOTP_H_
#define STM32_BOOTP_H_

#include "common_base.h"

//! Status messages
typedef enum stm32bp_status_te {
	STM32BP_STAT_OK = 0,
	STM32BP_STAT_READ_FAIL = -1,
	STM32BP_STAT_WRITE_FAIL = -2,
	STM32BP_STAT_NACK = -3,
	STM32BP_STAT_RDPROTECTED = -4,
	STM32BP_STAT_UNSUPPORTED = -5,
	STM32BP_STAT_OVERFLOW = -6,
	STM32BP_STAT_VERIFY_FAIL = -7
} stm32bp_status_t;

//! Erase flags
typedef enum stm32bp_erase_flags_te {
	STM32BP_NO_ERASE = 0,	///< Don't erase anything
	STM32BP_ERASE_ALL = 1,	///< Erase everything
	STM32BP_ERASE_BANK1 = 2,	///< Erase the whole Bank1
	STM32BP_ERASE_BANK2 = 4,	///< Erase the whole Bank2
	STM32BP_ERASE_NEEDED = 8	///< Only erase pages that need to be erased
} stm32bp_erase_flags_t;

/**
 * @note Callback functions are expected to be blocking.
 */

/**
 * Callback for reading the communications peripheral.
 * @param[out] data Pointer to the data buffer to read to.
 * @param[in] length Number of bytes expected.
 * @return Number of bytes read.
 */
extern int16_t stm32bp_read( uint8_t *data, uint16_t length );

/**
 * Callback for writing to the communications peripheral.
 * @param[out] data Pointer to the data buffer to write from.
 * @param[in] length Number of bytes to be written.
 * @return Number of bytes written.
 */
extern int16_t stm32bp_write( uint8_t *data, uint16_t length );

/**
 * Callback for setting the boot pin Low (0) or High (1).
 */
extern void stm32bp_set_boot( uint16_t value );

/**
 * Callback for pulsing the reset pin.
 */
extern void stm32bp_reset();

/**
 * Initializes communication with the bootloader.
 */
int16_t stm32bp_init();

/**
 * Deinitializes stuff.
 */
int16_t stm32bp_deinit();

/**
 * Removes write protect, causing the whole flash to be erased.
 */
int16_t stm32bp_wrun();

/**
 * Erases a block in the flash.
 */
int16_t stm32bp_erase( uint32_t address, uint32_t length, stm32bp_erase_flags_t erase_flags );

/**
 * Writes a block to flash.
 */
int16_t stm32bp_flash( uint32_t address, uint8_t *data, uint32_t length );

/**
 * Jumps the target to an address.
 */
int16_t stm32bp_go( uint32_t address );

#endif /* STM32_BOOTP_H_ */
