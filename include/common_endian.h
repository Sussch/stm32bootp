/**
 * @file common_endian.h
 *
 * @brief Endianness conversions
 *
 * @date Sep 12, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

#ifndef COMMON_ENDIAN_H_
#define COMMON_ENDIAN_H_

#include "common_base.h"

/**
 * Byte order for external devices
 */
typedef enum ec_endianness_te {
	EC_LITTLE_ENDIAN_16 = 0,	///< LSB, MSB
	EC_BIG_ENDIAN_16,	///< MSB, LSB
	EC_LITTLE_ENDIAN_32,	///< D C B A
	EC_BIG_ENDIAN_32	///< A B C D
} ec_endianness_t;

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
/**
 * Checks if the system is reverse-endian to another
 * device. @see ec_endianness_t.
 * @param[in] x device endianness.
 */
#define EC_REV_END( x )	( x == EC_BIG_ENDIAN_16 || x == EC_BIG_ENDIAN_32 )
//! \todo Determine if it's 16, 32 or 64 bit architecture
#define EC_SYSTEM_ENDIAN	EC_BIG_ENDIAN_32
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
/**
 * Checks if the system is reverse-endian to another
 * device. @see ec_endianness_t.
 * @param[in] x device endianness.
 */
#define EC_REV_END( x )	( x == EC_LITTLE_ENDIAN_16 || x == EC_LITTLE_ENDIAN_32 )
//! \todo Determine if it's 16, 32 or 64 bit architecture
#define EC_SYSTEM_ENDIAN	EC_LITTLE_ENDIAN_32
#endif

//! \todo Rename to SWAP_INDEXES_16
/**
 * Swaps the index for every second byte
 * @param[in] x Current byte index.
 */
#define EC_SWAP_BYTES_16( x )	( x % 2 == 0 ? x + 1 : x - 1 )

/**
 * Swaps the index for bytes in 32-bit words
 */
#define EC_SWAP_BYTES_32( x )	( ( x / 4 ) * 4 + 3 - x % 4 )

/**
 * Checks if the current byte index is that of
 * a stray byte (which shouldn't be swapped).
 */
#define EC_IS_STRAY_8( x, l )	( l % 2 != 0 && x == l - 1 )

/**
 * Checks if the current byte index is that of a stray half-word.
 */
#define EC_IS_STRAY_16( x, l )	( l % 4 != 0 && x == l - 2 )

/**
 * Swaps bytes, if needed.
 * Avoids swapping stray bytes.
 * @param[in] e Endianness. @see ec_endianness_t.
 * @param[in] x Current byte index.
 * @param[in] l Number of bytes in the array.
 */
#define EC_MAKE_ENDIAN_16( e, x, l )	\
	( ( EC_REV_END( e ) && !EC_IS_STRAY_8( x, l ) ) ? EC_SWAP_BYTES_16( x ) : x )

/**
 * Converts the endianness of a 16-bit integer.
 */
void ec_conv_endian16( ec_endianness_t e, uint16_t *x );
/**
 * Converts the endianness of a 32-bit integer.
 */
void ec_conv_endian32( ec_endianness_t e, uint32_t *x );
/**
 * Converts the endianness of an array.
 */
void ec_conv_array_endian( ec_endianness_t e, void *p, uint32_t len );

/**
 * Swaps bytes, if needed.
 * Avoids swapping stray bytes and half-words.
 * @param[in] e Endianness. @see ec_endianness_t.
 * @param[in] x Current byte index.
 * @param[in] l Number of bytes in the array.
 */
#define EC_MAKE_ENDIAN_32( e, x, l )	\
	( EC_REV_END( e ) ? \
	( ( !EC_IS_STRAY_16( x, l ) && !EC_IS_STRAY_16( x, l ) ) ? EC_SWAP_BYTES_32( x ) : EC_MAKE_ENDIAN_16( e, x, l ) ) \
	: x )

#endif /* COMMON_ENDIAN_H_ */
