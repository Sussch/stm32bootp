RELEASE = libstm32bootp.a
SRC = src/*.c
INC = include/*.h
GARBAGE = src/*.c~ include/*.h~ Makefile~ README.md~
OBJ = endian.o stm32_crc32.o stm32_bootp.o
CFLAGS = -Wall -s -O3 -I./include

# $< - source
# $@ - target
%.o: src/%.c
	gcc $< -c ${CFLAGS}

all: $(OBJ)
	ar rcs $(RELEASE) $(OBJ)

clean:
	rm -f $(RELEASE) $(OBJ) $(GARBAGE)

install: $(RELEASE)
	cp $(RELEASE) /usr/local/lib/
	mkdir /usr/local/include/stm32bootp
	cp $(INC) /usr/local/include/stm32bootp/

uninstall:
	rm /usr/local/lib/$(RELEASE)
	rm -Rf /usr/local/include/stm32bootp

