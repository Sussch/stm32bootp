/**
 * @file stm32_boot.c
 *
 * @brief STM32 internal bootloader protocol.
 *
 * @date Oct 3, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

#include "stm32_bootp.h"
#include "common_endian.h"

#include <stdio.h>

//#define DBG( x )	x
#ifndef DBG
#define DBG( x )
#endif

#define STM32BP_FLASH_START	0x08000000

//! STM32 bootprotocol commands and Ack, Nack bytes
typedef enum stm32bp_cmd_te {
	STM32BP_ACK = 0x79,
	STM32BP_NACK = 0x1F,

	STM32BP_INIT = 0x7F,	///< Initialize the bootloader.
	STM32BP_ID = 0x02,	///< Gets the chip ID.
	STM32BP_GET = 0x00,	///< Gets the version and the allowed commands supported
						///< by the current version of the bootloader.
	STM32BP_GETV = 0x01,	///< Get bootloader version and read protect info.
	STM32BP_GO = 0x21,	///< Jumps to user application code located in the internal
						///< Flash memory or in SRAM.
	STM32BP_READ = 0x11,	///< Reads up to 256 bytes of memory starting from an
							///< address specified by the application
	STM32BP_WRITE = 0x31,	///< Writes up to 256 bytes to the RAM or Flash memory
							///< starting from an address specified by the application.
	STM32BP_ERASE = 0x43,	///< Erases from one to all the Flash memory pages.
	STM32BP_EERASE = 0x44,	///< Erases from one to all the Flash memory pages using
							///< two byte addressing mode (available only for v3.0 usart
							///< bootloader versions and above).
	STM32BP_WRUNP = 0x73,	///< Disables write protection for all Flash memory sections.
	STM32BP_RDUNP = 0x92	///< Disables read protection for all Flash memory sections.
} stm32bp_cmd_t;

//! STM32 device configuration structure
typedef struct stm32bp_device_ts {
	uint16_t id;	///< Device ID

	uint32_t fl_size;	///< Flash size
	uint8_t fl_pps;	///< Number of pages per Flash sector
	uint16_t fl_psize;	///< Flash page size
} stm32bp_device_t;

//! Bootloader configuration structure
typedef struct stm32bp_bootloader_ts {
	uint8_t version;	///< Bootloader version
	uint8_t rdp_disabled;	///< Number of times read-protection has been disabled
	uint8_t rdp_enabled;	///< Number of times read-protection has been enabled
	uint8_t cmd_erase;	///< Erase command can be 0x43 or 0x44, depending on version
} stm32bp_bootloader_t;

//! Device configuration
stm32bp_device_t g_stm32bp_device;
//! Bootloader configuration
stm32bp_bootloader_t g_stm32bp_bootloader;

/**
 * Updates device configuration, based on device ID.
 */
inline int16_t stm32bp_dev_conf() {
	switch( g_stm32bp_device.id ) {
	// Low-density
	case 0x412:
		g_stm32bp_device.fl_size = 0x8000;
		g_stm32bp_device.fl_pps = 4;
		g_stm32bp_device.fl_psize = 1024;
		break;
	// Medium density
	case 0x410:
		g_stm32bp_device.fl_size = 0x20000;
		g_stm32bp_device.fl_pps = 4;
		g_stm32bp_device.fl_psize = 1024;
		break;
	// High density
	case 0x414:
		g_stm32bp_device.fl_size = 0x80000;
		g_stm32bp_device.fl_pps = 2;
		g_stm32bp_device.fl_psize = 2048;
		break;
	// Connectivity line
	case 0x418:
		g_stm32bp_device.fl_size = 0x40000;
		g_stm32bp_device.fl_pps = 2;
		g_stm32bp_device.fl_psize = 2048;
		break;
	// Medium density VL
	case 0x420:
		g_stm32bp_device.fl_size = 0x20000;
		g_stm32bp_device.fl_pps = 4;
		g_stm32bp_device.fl_psize = 1024;
		break;
	// XL density
	case 0x430:
		g_stm32bp_device.fl_size = 0x100000;
		g_stm32bp_device.fl_pps = 2;
		g_stm32bp_device.fl_psize = 2048;
		break;
	// F2
	case 0x411:
		g_stm32bp_device.fl_size = 0x100000;
		g_stm32bp_device.fl_pps = 4;
		g_stm32bp_device.fl_psize = 16384;
		break;
	// Unsupported device
	default:
		return STM32BP_STAT_UNSUPPORTED;
	}
	return STM32BP_STAT_OK;
}

inline int16_t stm32bp_send_cmd( stm32bp_cmd_t cmd ) {
	uint8_t buf[ 3 ] = { cmd, cmd ^ 0xFF, 0 };
	uint16_t n_bytes = 2;

	// Neglect checksum for INIT, ACK and NACK
	if (cmd == STM32BP_INIT ||
			cmd == STM32BP_ACK || cmd == STM32BP_NACK)
		n_bytes = 1;

	if (stm32bp_write( buf, n_bytes ) < n_bytes)
		return STM32BP_STAT_WRITE_FAIL;
	if (stm32bp_read( &buf[ 2 ], 1 ) < 1)
		return STM32BP_STAT_READ_FAIL;
	if (buf[ 2 ] != STM32BP_ACK)
		return STM32BP_STAT_NACK;
	return STM32BP_STAT_OK;
}

inline void stm32bp_prolog() {
	// Set BOOT0 High
	stm32bp_set_boot( 1 );
	// Pulse NRST
	stm32bp_reset();
}

inline void stm32bp_epilog() {
	// Set BOOT0 Low
	stm32bp_set_boot( 0 );
	// Pulse NRST
	stm32bp_reset();
}

inline int16_t stm32bp_read_ack() {
	uint8_t buf = 0;

	// Read ACK
	if (stm32bp_read( &buf, 1 ) != 1 )
		return STM32BP_STAT_READ_FAIL;
	if (buf != STM32BP_ACK)
		return STM32BP_STAT_NACK;
	return STM32BP_STAT_OK;
}

int16_t stm32bp_wrun() {
	int16_t retval = STM32BP_STAT_OK;

	DBG( printf( "> Disabling Write Protect\r\n" ); )
	// Disable Write protect, just in case
	retval = stm32bp_send_cmd( STM32BP_WRUNP );
	if (retval == STM32BP_STAT_OK) {
		DBG( printf( ">> Waiting for ACK\r\n" ); )
		// Read ACK
		retval = stm32bp_read_ack();
		if (retval != STM32BP_STAT_OK)
			return retval;
	} else
		return retval;

	//! \todo Delay for a couple of seconds here
	stm32bp_read( ( uint8_t * ) ( &retval ), 1 );

	DBG( printf( "> Reinitializing\r\n" ); )
	return stm32bp_init();
}

int16_t stm32bp_init() {
	uint8_t cmd = 0;
	uint8_t len = 0;
	int16_t retval = STM32BP_STAT_OK;
	uint16_t i;

	stm32bp_prolog();

	DBG( printf( "> Sending INIT\r\n" ); )
	// Initialize
	retval = stm32bp_send_cmd( STM32BP_INIT );
	if (retval == STM32BP_STAT_OK) {
		DBG( printf( "> Sending GET\r\n" ); )
		// Get bootloader version and list of supported commands
		retval = stm32bp_send_cmd( STM32BP_GET );
		if (retval == STM32BP_STAT_OK ) {
			DBG( printf( ">> Reading no. of bytes\r\n" ); )
			// Read number of bytes
			if (stm32bp_read( &len, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> %d\r\n", len ); )
			DBG( printf( ">> Reading version\r\n" ); )
			// Read bootloader version
			if (stm32bp_read( &g_stm32bp_bootloader.version, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> 0x%X\r\n", g_stm32bp_bootloader.version ); )
			DBG( printf( ">> Reading list of supported commands\r\n" ); )
			// Read list of supported commands
			for (i=0; i<len; i++) {
				if (stm32bp_read( &cmd, 1 ) != 1 )
					return STM32BP_STAT_READ_FAIL;
				DBG( printf( ">>> 0x%0X\r\n", cmd ); )
				// Which erase command is supported?
				if (cmd == 0x43)
					g_stm32bp_bootloader.cmd_erase = 0x43;
				else if (cmd == 0x44)
					g_stm32bp_bootloader.cmd_erase = 0x44;

			}
			DBG( printf( ">> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;
		} else
			return retval;

		DBG( printf( "> Fetching Read Protection status\r\n" ); )
		// Read Read protection status
		retval = stm32bp_send_cmd( STM32BP_GETV );
		if (retval == STM32BP_STAT_OK ) {
			DBG( printf( ">> Reading Bootloader version\r\n" ); )
			// Dummy-read bootloader version
			if (stm32bp_read( &cmd, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> 0x%X\r\n", cmd ); )
			DBG( printf( ">> Reading no. of Read Protection disables\r\n" ); )
			// Read the number of times read-protect has been disabled
			if (stm32bp_read( &g_stm32bp_bootloader.rdp_disabled, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> %d\r\n", g_stm32bp_bootloader.rdp_disabled ); )
			DBG( printf( ">> Reading no. of Read Protection enables\r\n" ); )
			// Read the number of times read-protect has been enabled
			if (stm32bp_read( &g_stm32bp_bootloader.rdp_enabled, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> %d\r\n", g_stm32bp_bootloader.rdp_disabled ); )
			DBG( printf( ">> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;

			// Probably read-protected?
			if (g_stm32bp_bootloader.rdp_disabled <
					g_stm32bp_bootloader.rdp_enabled)
				retval = STM32BP_STAT_RDPROTECTED;
		} else
			return retval;

		DBG( printf( "> Reading Device PID\r\n" ); )
		// Read device PID
		retval = stm32bp_send_cmd( STM32BP_ID );
		if (retval == STM32BP_STAT_OK ) {
			DBG( printf( ">> Reading no. of bytes\r\n" ); )
			// Number of bytes
			if (stm32bp_read( &len, 1 ) != 1 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> %d\r\n", len ); )
			// Unsupported device?
			if (len > 1)
				return STM32BP_STAT_UNSUPPORTED;
			DBG( printf( ">> Reading PID\r\n" ); )
			// Read PID
			if (stm32bp_read( ( ( uint8_t *) &g_stm32bp_device.id ), len + 1 ) != 2 )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> 0x%04X\r\n", g_stm32bp_device.id ); )
			DBG( printf( ">> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;

			DBG( printf( ">> Device PID in BigEndian16\r\n" ); )
			// Got PID in Big Endian
			ec_conv_endian16( EC_BIG_ENDIAN_16, &g_stm32bp_device.id );
			DBG( printf( ">>> 0x%04X\r\n", g_stm32bp_device.id ); )

			DBG( printf( ">> Device Configuration\r\n" ); )
			// Update device configuration
			retval = stm32bp_dev_conf();
			DBG( printf( ">>> Flash size: %d KB\r\n", g_stm32bp_device.fl_size / 1024 ); )
			DBG( printf( ">>> Flash page size: %d KB\r\n", g_stm32bp_device.fl_psize / 1024 ); )
			DBG( printf( ">>> Flash pages per sector: %d\r\n", g_stm32bp_device.fl_pps ); )
		}
	}

	DBG( printf( ">Init done\r\n" ); )

	return retval;
}

int16_t stm32bp_deinit() {
	stm32bp_epilog();
	return STM32BP_STAT_OK;
}

int16_t stm32bp_erase( uint32_t address, uint32_t length, stm32bp_erase_flags_t erase_flags ) {
	uint16_t buf16[ 4 ];
	uint8_t buf[ 4 ];
	int16_t retval = STM32BP_STAT_OK;

	DBG( printf( ">Erasing 0x%08X [0x%X]\r\n", address, length ); )

	address -= STM32BP_FLASH_START;

	if (erase_flags == STM32BP_NO_ERASE)
		return retval;

	// Perform a full erase, in case the target is read-protected
	if (g_stm32bp_bootloader.rdp_disabled <
			g_stm32bp_bootloader.rdp_enabled) {
		DBG( printf( ">Full erase due to Read-Protection\r\n" ); )
		DBG( printf( ">Sending Read-Unprotect\r\n" ); )
		// Erase all the Flash, disabling Read protect (just in case)
		retval = stm32bp_send_cmd( STM32BP_RDUNP );
		if (retval == STM32BP_STAT_OK) {
			DBG( printf( ">>Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;
		} else
			return retval;
	} else {
		DBG( printf( "> Sending Erase command\r\n" ); )
		// Erase command
		retval = stm32bp_send_cmd( g_stm32bp_bootloader.cmd_erase );
		if (retval != STM32BP_STAT_OK)
			return retval;

		// 0x43?
		if (g_stm32bp_bootloader.cmd_erase == 0x43) {
			DBG( printf( ">> Standard Erase\r\n" ); )
			// Full erase or an attempt to bank-erase (only supported on extended erase)?
			if (erase_flags == STM32BP_ERASE_ALL ||
					erase_flags == STM32BP_ERASE_BANK1 ||
					erase_flags == STM32BP_ERASE_BANK2) {
				DBG( printf( ">>> Erasing all\r\n" ); )
				buf[ 0 ] = 0xFF;
				buf[ 3 ] = 0x00;

				if (stm32bp_write( buf, 1 ) < 1 )
					return STM32BP_STAT_WRITE_FAIL;
			// Only erase pages that need to be erased
			} else if (erase_flags == STM32BP_ERASE_NEEDED) {
				DBG( printf( ">>> Erasing only needed pages\r\n" ); )
				// First page
				buf[ 0 ] = ( uint8_t ) ( address / g_stm32bp_device.fl_psize );
				// Last page
				buf[ 2 ] = ( uint8_t ) ( ( address + length ) / g_stm32bp_device.fl_psize );
				// Number of pages to be erased
				buf[ 1 ] = ( uint8_t ) ( length / g_stm32bp_device.fl_psize );
				if (buf[ 1 ] == 0)
					buf[ 1 ] = 1;
				// Checksum
				buf[ 3 ] = buf[ 1 ];

				DBG( printf( ">>>> Sending number of pages (%d)\r\n", buf[ 1 ] ); )
				// Send the number of pages
				if (stm32bp_write( &buf[ 1 ], 1 ) < 1 )
					return STM32BP_STAT_WRITE_FAIL;
				DBG( printf( ">>>> Sending page numbers (%d - %d)\r\n", buf[ 0 ], buf[ 2 ] ); )
				// Send page numbers for erasure
				for (buf[ 1 ] = buf[ 0 ]; buf[ 1 ] <= buf[ 2 ]; buf[ 1 ]++) {
					DBG( printf( ">>>>%d\r\n", buf[ 1 ] ); )
					// XOR together for the checksum
					buf[ 3 ] ^= buf[ 1 ];
					if (stm32bp_write( &buf[ 1 ], 1 ) < 1 )
						return STM32BP_STAT_WRITE_FAIL;
				}
			}
		// Extended erase with 0x44?
		} else {
			DBG( printf( ">> Extended erase\r\n" ); )
			// Full erase?
			if (erase_flags == STM32BP_ERASE_ALL) {
				DBG( printf( ">>> Erasing all\r\n" ); )

				buf[ 0 ] = 0xFF;
				buf[ 1 ] = 0xFF;
				// Checksum, XOR of the 2 bytes
				buf[ 3 ] = 0x00;

				if (stm32bp_write( buf, 2 ) < 2 )
					return STM32BP_STAT_WRITE_FAIL;
			} else if (erase_flags == STM32BP_ERASE_BANK1) {
				DBG( printf( ">>> Erasing Bank1\r\n" ); )

				buf[ 0 ] = 0xFF;
				buf[ 1 ] = 0xFE;
				// Checksum, XOR of the 2 bytes
				buf[ 3 ] = 0x01;

				if (stm32bp_write( buf, 2 ) < 2 )
					return STM32BP_STAT_WRITE_FAIL;
			} else if (erase_flags == STM32BP_ERASE_BANK2) {
				DBG( printf( ">>> Erasing Bank2\r\n" ); )

				buf[ 0 ] = 0xFF;
				buf[ 1 ] = 0xFD;
				// Checksum, XOR of the 2 bytes
				buf[ 3 ] = 0x02;

				if (stm32bp_write( buf, 2 ) < 2 )
					return STM32BP_STAT_WRITE_FAIL;
			} else if (erase_flags == STM32BP_ERASE_NEEDED) {
				DBG( printf( ">>> Erasing only needed pages\r\n" ); )
				// First page
				buf16[ 0 ] = ( uint16_t ) ( address / g_stm32bp_device.fl_psize );
				// Last page
				buf16[ 2 ] = ( uint16_t ) ( ( address + length ) / g_stm32bp_device.fl_psize );
				// Number of pages to be erased
				buf16[ 1 ] = ( uint16_t ) ( length / g_stm32bp_device.fl_psize );
				if (buf16[ 1 ] == 0)
					buf16[ 1 ] = 1;
				// Checksum
				buf[ 3 ] = ( ( uint8_t * ) ( &buf16[ 1 ] ) )[ 0 ] ^ ( ( uint8_t * ) ( &buf16[ 1 ] ) )[ 1 ];

				DBG( printf( ">>>> Sending number of pages (%d)\r\n", buf16[ 1 ] ); )

				// We need Big Endian
				ec_conv_endian16( EC_BIG_ENDIAN_16, &buf16[ 0 ] );
				ec_conv_endian16( EC_BIG_ENDIAN_16, &buf16[ 1 ] );
				ec_conv_endian16( EC_BIG_ENDIAN_16, &buf16[ 2 ] );

				// Send the number of pages
				if (stm32bp_write( ( uint8_t * ) ( &buf16[ 1 ] ), 2 ) < 2 )
					return STM32BP_STAT_WRITE_FAIL;
				DBG( printf( ">>>> Sending page numbers (%d - %d)\r\n", buf16[ 0 ], buf16[ 2 ] ); )
				// Send page numbers for erasure
				for (buf16[ 1 ] = buf16[ 0 ]; buf16[ 1 ] <= buf16[ 2 ]; buf16[ 1 ]++) {
					// XOR together for the checksum
					buf[ 3 ] ^= ( ( uint8_t * ) ( &buf16[ 1 ] ) )[ 0 ] ^ ( ( uint8_t * ) ( &buf16[ 1 ] ) )[ 1 ];
					DBG( printf( ">>>>%d\r\n", buf16[ 1 ] ); )
					// Convert to Big Endian
					ec_conv_endian16( EC_BIG_ENDIAN_16, &buf16[ 1 ] );
					if (stm32bp_write( ( uint8_t * ) ( &buf16[ 1 ] ), 2 ) < 2 )
						return STM32BP_STAT_WRITE_FAIL;
				}
			}
		}
		DBG( printf( ">> Sending checksum (0x%02X)\r\n", buf[ 3 ] ); )
		// Write checksum
		if (stm32bp_write( &buf[ 3 ], 1 ) != 1 )
			return STM32BP_STAT_WRITE_FAIL;
		DBG( printf( ">> Waiting for ACK\r\n" ); )
		// Wait for ACK
		retval = stm32bp_read_ack();
		if (retval != STM32BP_STAT_OK)
			return retval;
	}

	DBG( printf( "> Done erasing.\r\n" ); )
	return retval;
}

int16_t stm32bp_flash( uint32_t address, uint8_t *data, uint32_t length ) {
	uint8_t *_data = data;
	uint32_t _address = address;
	uint8_t cksum = 0, pad = 0xFF, len = 0;
	uint8_t buf[ 256 ];
	int16_t retval = STM32BP_STAT_OK;
	uint32_t i, j, _len = 0, _flen = 0;

	DBG( printf( "> Flashing\r\n" ); )

	if (length > g_stm32bp_device.fl_size)
		return STM32BP_STAT_OVERFLOW;

	// We can write 256 bytes at a time
	for (i=0; i<length; i+=256) {
		// Step writing address
		_address = address + i;
		_data = data + i;
		// We need the address in Big Endian byte order
		ec_conv_endian32( EC_BIG_ENDIAN_32, &_address );

		DBG( printf( ">> Sending Write command\r\n" ); )
		// Write stuff
		retval = stm32bp_send_cmd( STM32BP_WRITE );
		if (retval == STM32BP_STAT_OK) {
			DBG( printf( ">>> Writing address (0x%08X, 0x%08X)\r\n", address + i, _address ); )
			// Write address
			if (stm32bp_write( ( uint8_t * ) ( &_address ), 4 ) != 4 )
				return STM32BP_STAT_WRITE_FAIL;
			// Write address checksum
			cksum = ( ( uint8_t * ) &_address )[ 0 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 1 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 2 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 3 ];
			DBG( printf( ">>> Writing checksum (0x%02X)\r\n", cksum ); )
			if (stm32bp_write( &cksum, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;

			// Number of bytes to be written here
			_len = EC_MIN( length - i, 256 );
			// Note that it must be a multiple of 4 bytes
			_flen = _len;
			if (_len % 4 > 0)
				_flen += ( _len % 4 );
			len = _flen - 1;
			cksum = len;
			DBG( printf( ">>> Writing chunk length (%d of %d bytes), 0x%02X\r\n", _flen, _len, len ); )
			// Write it
			if (stm32bp_write( &len, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Writing data\r\n>>>> " ); )
			// Calculate the checksum
			for (j=0; j<_flen; j++) {
				DBG( printf( "0x%02X ", *( _data + j ) ); )
				cksum ^= *( _data + j );
			}
			DBG( printf( "\r\n" ); )
			// Write data
			if (stm32bp_write( _data, _flen ) != _flen )
				return STM32BP_STAT_WRITE_FAIL;
			if (_flen - _len > 0) {
				DBG( printf( ">>> Padding with %d bytes\r\n>>>> ", _flen - _len ); )
				// Write padding
				for (j=_flen; j<_len; j++) {
					cksum ^= 0xFF;
					DBG( printf( "0xFF " ); )
					if (stm32bp_write( &pad, 1 ) != 1 )
						return STM32BP_STAT_WRITE_FAIL;
				}
				DBG( printf( "\r\n" ); )
			}
			DBG( printf( ">>> Writing checksum (0x%02X)\r\n", cksum ); )
			// Write checksum
			if (stm32bp_write( &cksum, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;
		} else
			return retval;

		DBG( printf( ">> Sending Read command\r\n" ); )
		// Read stuff
		retval = stm32bp_send_cmd( STM32BP_READ );
		if (retval == STM32BP_STAT_OK) {
			DBG( printf( ">>> Writing address (0x%08X, 0x%08X)\r\n", address + i, _address ); )
			// Write address
			if (stm32bp_write( ( uint8_t * ) ( &_address ), 4 ) != 4 )
				return STM32BP_STAT_WRITE_FAIL;
			// Write address checksum
			cksum = ( ( uint8_t * ) &_address )[ 0 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 1 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 2 ];
			cksum ^= ( ( uint8_t * ) &_address )[ 3 ];
			DBG( printf( ">>> Writing checksum (0x%02X)\r\n", cksum ); )
			if (stm32bp_write( &cksum, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;

			// Number of bytes to be read here
			_len = EC_MIN( length - i, 256 );
			len = _len - 1;
			cksum = ~len;
			DBG( printf( ">>> Writing chunk length (%d bytes), 0x%02X\r\n", _len, len ); )
			// Write it
			if (stm32bp_write( &len, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Writing checksum (0x%02X)\r\n", cksum ); )
			// Write checksum
			if (stm32bp_write( &cksum, 1 ) != 1 )
				return STM32BP_STAT_WRITE_FAIL;
			DBG( printf( ">>> Waiting for ACK\r\n" ); )
			// Read ACK
			retval = stm32bp_read_ack();
			if (retval != STM32BP_STAT_OK)
				return retval;
			DBG( printf( ">>> Reading data\r\n" ); )
			// Read data
			if (stm32bp_read( buf, _len ) != _len )
				return STM32BP_STAT_READ_FAIL;
			DBG( printf( ">>> Verifying it\r\n" ); )
			// Verify it
			for (j=0; j<_len; j++) {
				//DBG( printf( ">>>> %d. 0x%02X 0x%02X\r\n", j, _data[ j ], buf[ j ] ); )
				if (_data[ j ] != buf[ j ])
					return STM32BP_STAT_VERIFY_FAIL;
			}
		} else
			return retval;
	}

	DBG( printf( "> Done Flashing\r\n" ); )
	return retval;
}

int16_t stm32bp_go( uint32_t address ) {
	uint32_t _address = address;
	uint8_t cksum = 0;
	int16_t retval = STM32BP_STAT_OK;

	DBG( printf( "> Jumping to address 0x%08X\r\n", address ); )
	// Go!
	retval = stm32bp_send_cmd( STM32BP_GO );
	if (retval == STM32BP_STAT_OK) {
		// Need the address in Big Endian
		ec_conv_endian32( EC_BIG_ENDIAN_32, &_address );
		DBG( printf( ">> Writing address (0x%08X, 0x%08X)\r\n", address, _address ); )
		// Write address
		if (stm32bp_write( ( uint8_t * ) ( &_address ), 4 ) != 4 )
			return STM32BP_STAT_WRITE_FAIL;
		// Write address checksum
		cksum = ( ( uint8_t * ) &_address )[ 0 ];
		cksum ^= ( ( uint8_t * ) &_address )[ 1 ];
		cksum ^= ( ( uint8_t * ) &_address )[ 2 ];
		cksum ^= ( ( uint8_t * ) &_address )[ 3 ];
		DBG( printf( ">> Writing checksum (0x%02X)\r\n", cksum ); )
		if (stm32bp_write( &cksum, 1 ) != 1 )
			return STM32BP_STAT_WRITE_FAIL;
		DBG( printf( ">> Waiting for ACK\r\n" ); )
		// Read ACK
		retval = stm32bp_read_ack();
		if (retval != STM32BP_STAT_OK)
			return retval;
		DBG( printf( ">> Waiting for ACK\r\n" ); )
		// Read ACK
		retval = stm32bp_read_ack();
		if (retval != STM32BP_STAT_OK)
			return retval;
	} else
		return retval;

	DBG( printf( "> Done jumping\r\n" ); )
	return retval;
}
