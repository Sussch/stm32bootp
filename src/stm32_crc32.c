/**
 * @file stm32_crc32.c
 *
 * @brief CRC-32, compatible with STM32 hardware implementation.
 *
 * @date Nov 6, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

#include "stm32_crc32.h"

#include <stdio.h>

/**
 * Thanks to clive1 at
 * https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?
 * RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2Fcortex_mx_stm32%2FCRC%20computation&
 * FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=2688
 */
unsigned int crc32_1( unsigned int crc, unsigned int data ) {
	int i;

	crc = crc ^ data;

	for (i=0; i<32; i++) {
		if (crc & 0x80000000)
			crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
		else
			crc = (crc << 1);
	}

	return crc;
}

/**
 * Thanks to clive1 at
 * https://my.st.com/public/STe2ecommunities/mcu/Lists/cortex_mx_stm32/Flat.aspx?
 * RootFolder=%2Fpublic%2FSTe2ecommunities%2Fmcu%2FLists%2Fcortex_mx_stm32%2FCRC%20computation&
 * FolderCTID=0x01200200770978C69A1141439FE559EB459D7580009C4E14902C3CDE46A77F0FFD06506F5B&currentviews=2688
 */
unsigned int crc32_1f( unsigned int crc, unsigned int data ) {
	// Nibble lookup table for 0x04C11DB7 polynomial
	static const unsigned int crc_table[ 16 ] = {
	0x00000000,0x04C11DB7,0x09823B6E,0x0D4326D9,0x130476DC,0x17C56B6B,0x1A864DB2,0x1E475005,
	0x2608EDB8,0x22C9F00F,0x2F8AD6D6,0x2B4BCB61,0x350C9B64,0x31CD86D3,0x3C8EA00A,0x384FBDBD };

	crc = crc ^ data; // Apply all 32-bits

	// Process 32-bits, 4 at a time, or 8 rounds
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ]; // Assumes 32-bit reg, masking index to 4-bits
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ]; //  0x04C11DB7 Polynomial used in STM32
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];
	crc = ( crc << 4 ) ^ crc_table[ crc >> 28 ];

	return crc;
}

unsigned int crc32_block( unsigned int crc, unsigned int *data, unsigned int length ) {
	unsigned int i;

	for (i=0; i<length; i++)
		crc = crc32_1f( crc, data[ i ] );

	return crc;
}
