/**
 * @file endian.c
 *
 * @brief Endianness conversions
 *
 * @date Nov 9, 2012
 * @author Indrek
 * @copyright Tartu Observatory
 */

#include "common_endian.h"

void ec_conv_endian16( ec_endianness_t e, uint16_t *x ) {
	uint8_t c;

	if (EC_REV_END( e )) {
		c = ( ( uint8_t * ) x )[ 0 ];
		( ( uint8_t * ) x )[ 0 ] = ( ( uint8_t * ) x )[ 1 ];
		( ( uint8_t * ) x )[ 1 ] = c;
	}
}

void ec_conv_endian32( ec_endianness_t e, uint32_t *x ) {
	uint16_t c;

	if (EC_REV_END( e )) {
		c = ( ( uint16_t * ) x )[ 0 ];
		( ( uint16_t * ) x )[ 0 ] = ( ( uint16_t * ) x )[ 1 ];
		ec_conv_endian16( e, &( ( ( uint16_t * ) x )[ 0 ] ) );
		ec_conv_endian16( e, &c );
		( ( uint16_t * ) x )[ 1 ] = c;
	}
}

inline void ec_conv_array_endian16( ec_endianness_t e, void *p, uint32_t len ) {
	uint8_t *bp = ( uint8_t * ) p;
	uint32_t i, slen;

	slen = ( len / 2 ) * 2;
	for (i=0; i<slen; i+=2)
		ec_conv_endian16( e, ( uint16_t * ) &bp[ i ] );
}

inline void ec_conv_array_endian32( ec_endianness_t e, void *p, uint32_t len ) {
	uint8_t *bp = ( uint8_t * ) p;
	uint32_t i, wlen;

	wlen = ( len / 4 ) * 4;
	for (i=0; i<wlen; i+=4)
		ec_conv_endian32( e, ( uint32_t * ) &bp[ i ] );
	if (len - wlen >= 2)
		ec_conv_endian16( e, ( uint16_t * ) &bp[ i ] );
}

void ec_conv_array_endian( ec_endianness_t e, void *p, uint32_t len ) {
	switch( e ) {
	case EC_LITTLE_ENDIAN_16:
	case EC_BIG_ENDIAN_16:
		ec_conv_array_endian16( e, p, len );
		break;
	case EC_LITTLE_ENDIAN_32:
	case EC_BIG_ENDIAN_32:
		ec_conv_array_endian32( e, p, len );
		break;
	default:
		break;
	}
}
