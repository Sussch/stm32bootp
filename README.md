# STM32 bootloader protocol library

An implementation of the [STM32 ARM][1] processor bootloader protocol.

Initially meant for reprogramming one of these processors from another.

## License
STM32bootp is distributed under the [LGPLv3 license][2].

[1]: http://www.st.com/internet/mcu/class/1734.jsp
[2]: http://www.gnu.org/copyleft/lesser.html
